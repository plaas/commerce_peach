<?php

namespace Drupal\commerce_peach\PluginForm\OffsiteRedirect;

use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentMethodFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;

/**
 * {@inheritdoc}
 */
class PaymentOffsiteForm extends BasePaymentOffsiteForm {

  protected $integrityHash;

  /**
   * Builds the credit card form.
   *
   * @param array $element
   *   The target element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   *
   * @return array
   *   The built credit card form.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $payment = $this->entity;
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $conf = $payment_gateway_plugin->getConfiguration();

    $peach_url = $conf['peach_test_url'];
    $entity_id = $conf['test_entity_id'];
    $access_token = $conf['test_access_token'];

    if ($conf['transaction_mode'] == 'live') {
      $peach_url = $conf['peach_live_url'];
      $entity_id = $conf['live_entity_id'];
      $access_token = $conf['live_access_token'];
    }
    
    $order = $payment->getOrder();
    $order->save();

    $order_total = number_format((float)$payment->getAmount()->getNumber()*100., 0, '.', '');

    $data = [];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, sprintf("%s/v1/checkouts", $peach_url));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(sprintf('Authorization:Bearer %s', $access_token)));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, sprintf("entityId=%s&amount=%s&currency=%s&paymentType=DB", $entity_id, $order_total, $conf['transaction_currency']));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $conf['transaction_mode'] == 'live' ? true : false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $responseData = json_decode(curl_exec($ch));

    if(curl_errno($ch)) {
      return curl_error($ch);
    }

    curl_close($ch);

    //drupal_set_message('<pre>'. print_r($responseData, true) .'</pre>');

    if (isset($responseData->id)) {
      $checkoutId = $responseData->id;

      //$form['#attributes']['data-brands'] = $conf['transaction_brands'];
      //$form['#attributes']['class'] = ['paymentWidgets'];

      $options = [
        "peach_url" => sprintf('%s/v1/paymentWidgets.js?checkoutId=%s', $peach_url, $checkoutId),
        "redirect_url" => $form['#return_url'],
        "class" => 'paymentWidgets',
        "brands" => $conf['transaction_brands'],
        "complete" => false,
      ];

      if (isset($_REQUEST['id'])) {
        $options['complete'] = true;
      }

      $form['#attached']['drupalSettings']['commerce_peach'] = $options;
      $form['#attached']['library'][] = 'commerce_peach/peach';

      $form['#action'] = $form['#return_url'];
    }
    else {
      drupal_set_message('An error has occurred', 'error');
    }

    return $form;
  }
}
