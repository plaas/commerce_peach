<?php

namespace Drupal\commerce_peach\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;

/**
 * Provides the interface for the Rave payment gateway.
 */
interface PeachCheckoutInterface extends OffsitePaymentGatewayInterface {
  /**
   * Gets the redirect URL.
   *
   * @return string
   *   The redirect URL.
   */
  public function getRedirectUrl();

  /**
   * Get the configured Rave Payment Flow mode.
   *
   * @return string
   *   The Rave Payment Flow mode.
   */
  public function getPaymentFlow();
}
