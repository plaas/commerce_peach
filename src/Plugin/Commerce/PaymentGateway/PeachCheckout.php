<?php

namespace Drupal\commerce_peach\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_peach\Event\PeachCheckoutRequestEvent;
use Drupal\commerce_peach\Event\PeachEvents;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_price\Price;
use Drupal\commerce_price\RounderInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;

/**
 * Provides the Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(

 *   id = "peach_checkout",
 *   label = @Translation("Peach Payments"),
 *   display_label = @Translation("Peach Payments"),
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_peach\PluginForm\OffsiteRedirect\PaymentOffsiteForm",
 *   },
 *   modes= {
 *     "staging" = "Staging",
 *     "live" = "Live"
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "dinersclub", "discover", "jcb", "maestro", "mastercard", "visa",
 *   }
 * )
 */
class PeachCheckout extends OffsitePaymentGatewayBase implements PeachCheckoutInterface {
  const PEACH_TEST_URL = 'https://test.oppwa.com';
  const PEACH_LIVE_URL = 'https://oppwa.com';
  const PEACH_CURRENCY = 'ZAR';

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The price rounder.
   *
   * @var \Drupal\commerce_price\RounderInterface
   */
  protected $rounder;

  /**
   * The time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructs a new PaymentGatewayBase object.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, LoggerChannelFactoryInterface $logger_channel_factory, ClientInterface $client, RounderInterface $rounder, ModuleHandlerInterface $module_handler, EventDispatcherInterface $event_dispatcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

    $this->logger = $logger_channel_factory->get('commerce_peach');
    $this->httpClient = $client;
    $this->rounder = $rounder;
    $this->moduleHandler = $module_handler;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('logger.factory'),
      $container->get('http_client'),
      $container->get('commerce_price.rounder'),
      $container->get('module_handler'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {

  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'test_user_id' => '',
      'test_password' => '',
      'test_entity_id' => '',
      'test_access_token' => '',

      'live_user_id' => '',
      'live_password' => '',
      'live_entity_id' => '',
      'live_access_token' => '',

      'payment_flow' => 'iframe',

      'transaction_mode' => 'test',
      'transaction_currency' => self::PEACH_CURRENCY,
      'transaction_brands' => 'VISA MASTER AMEX',

      'peach_test_url' => self::PEACH_TEST_URL,
      'peach_live_url' => self::PEACH_LIVE_URL,

      'redirect_validation_hash' => $this->_commerce_peach_randomstring(16),
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['test_user_id'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Test User ID'),
      '#description' => $this->t('Your test user Id.'),
      '#default_value' => $this->configuration['test_user_id'],
    );

    $form['test_password'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Test Password'),
      '#description' => $this->t('Your test password.'),
      '#default_value' => $this->configuration['test_password'],
    );

    $form['test_entity_id'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Test Entity ID'),
      '#description' => $this->t('Your test entity Id.'),
      '#default_value' => $this->configuration['test_entity_id'],
    );

    $form['test_access_token'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Test Access Token'),
      '#description' => $this->t('Your test test access token.'),
      '#default_value' => $this->configuration['test_access_token'],
    );

    $form['live_user_id'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Live User ID'),
      '#description' => $this->t('Your live user Id.'),
      '#default_value' => $this->configuration['live_user_id'],
    );

    $form['live_password'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Live Password'),
      '#description' => $this->t('Your live password.'),
      '#default_value' => $this->configuration['live_password'],
    );

    $form['live_entity_id'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Live Entity ID'),
      '#description' => $this->t('Your live entity Id.'),
      '#default_value' => $this->configuration['live_entity_id'],
    );

    $form['live_access_token'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Live Access Token'),
      '#description' => $this->t('Your live test access token.'),
      '#default_value' => $this->configuration['live_access_token'],
    );

    $form['payment_flow'] = [
      '#type' => 'radios',
      '#title' => $this->t('Payment flow'),
      '#options' => ['iframe' => $this->t('iFrame')],
      '#default_value' => $this->getPaymentFlow(),
      '#required' => FALSE,
    ];

    $form['transaction_mode'] = array(
      '#type' => 'select',
      '#title' => $this->t('Transaction mode'),
      '#description' => $this->t('Test is development server, live will process transactions'),
      '#options' => array(
        'test' => $this->t('Test - Development Mode'),
        'live' => $this->t('Live - Production Mode'),
      ),
      '#multiple' => FALSE,
      '#default_value' => $this->configuration['transaction_mode'],
    );

    $form['transaction_currency'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Transaction Currency'),
      '#description' => $this->t('Currency to use for transactions.'),
      '#default_value' => $this->configuration['transaction_currency'],
      '#disabled' => true,
    );

    $form['transaction_brands'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Transaction Currency'),
      '#description' => $this->t('Supported transaction brands.'),
      '#default_value' => $this->configuration['transaction_brands'],
    );

    $form['peach_test_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Test URL'),
      '#description' => $this->t('Peach test URL'),
      '#default_value' => $this->configuration['peach_test_url'],
      '#disabled' => true,
    );

    $form['peach_live_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Live URL'),
      '#description' => $this->t('Peach live URL'),
      '#default_value' => $this->configuration['peach_live_url'],
      '#disabled' => true,
    );

    $form['redirect_validation_hash'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Redirect Validation Hash'),
      '#description' => $this->t(''),
      '#default_value' => $this->configuration['redirect_validation_hash'],
      '#disabled' => true,
    );
    
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);

      $this->configuration['test_user_id'] = $values['test_user_id'];
      $this->configuration['test_password'] = $values['test_password'];
      $this->configuration['test_entity_id'] = $values['test_entity_id'];
      $this->configuration['test_access_token'] = $values['test_access_token'];

      $this->configuration['live_user_id'] = $values['live_user_id'];
      $this->configuration['live_password'] = $values['live_password'];
      $this->configuration['live_entity_id'] = $values['live_entity_id'];
      $this->configuration['live_access_token'] = $values['live_access_token'];

      $this->configuration['payment_flow'] = $values['payment_flow'];
      
      $this->configuration['transaction_mode'] = $values['transaction_mode'];
      $this->configuration['transaction_currency'] = $values['transaction_currency'];
      $this->configuration['transaction_brands'] = $values['transaction_brands'];

      $this->configuration['peach_test_url'] = $values['peach_test_url'];
      $this->configuration['peach_live_url'] = $values['peach_live_url'];

      $this->configuration['redirect_validation_hash'] = $values['redirect_validation_hash'];
    }
  }

  public function _get_peach_response($id) {
    $checkoutId = $id;

    $success_codes = array(
      '000.000.000',
      '000.100.110',
      '000.100.111',
      '000.100.112',
    );

    $peach_url = $this->configuration['peach_test_url'];
    $entity_id = $this->configuration['test_entity_id'];
    $access_token = $this->configuration['test_access_token'];
    if ($this->configuration['transaction_mode'] == 'live') {
      $peach_url = $this->configuration['peach_live_url'];
      $entity_id = $this->configuration['live_entity_id'];
      $access_token = $this->configuration['live_access_token'];
    }

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, sprintf("%s/v1/checkouts/%s/payment?entityId=%s", $peach_url, $checkoutId, $entity_id));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(sprintf('Authorization:Bearer %s', $access_token)));
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->configuration['transaction_mode'] == 'live' ? true : false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $responseData = json_decode(curl_exec($ch));

    if(curl_errno($ch)) {
      return curl_error($ch);
    }

    curl_close($ch);

    $responseData->failed = false;
    if (!in_array($responseData->result->code, $success_codes)) {
      $responseData->failed = true;
    }

    $responseData->checkoutId = $checkoutId;

    return $responseData;
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $logger = \Drupal::logger('commerce_peach');

    $payment_id = urldecode($request->query->get('id'));
    $payment_resource = urldecode($request->query->get('resourcePath'));

    $logger->info('Peach returned id: ' . $payment_id);

    $response = $this->_get_peach_response($payment_id);

    //drupal_set_message('<pre>'. print_r($response, true) .'</pre>');

    if (!$response->failed) {
      $orderAmount = $order->getTotalPrice()->getNumber();

      $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
      $payment = $payment_storage->create([
        'state' => 'authorization',
        'amount' => $order->getTotalPrice(),
        'payment_gateway' => $this->entityId,
        'order_id' => $order->id(),
        'remote_id' => $payment_id,
        'remote_state' => $responseData->result->code,
      ]);

      $logger->info('Saving Payment information. Transaction reference: ' . $merchantTransactionReference);

      $payment->save();
      drupal_set_message('Payment was processed');

      $logger->info('Payment information saved successfully. Transaction reference: ' . $merchantTransactionReference);
    }
    else {
      throw new PaymentGatewayException('Payment was not successful.');
    }
  }

  public function onCancel(OrderInterface $order, Request $request) {
    $response = $this->_get_peach_response($_POST);
    drupal_set_message($response->result->description, 'warning');
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaymentInstructions(PaymentInterface $payment) {
    $instructions = [];
    if (!empty($this->configuration['instructions']['value'])) {
      $instructions_text = $this->token->replace($this->configuration['instructions']['value'], [
        'commerce_order' => $payment->getOrder(),
        'commerce_payment' => $payment,
      ]);

      $instructions = [
        '#type' => 'processed_text',
        '#text' => $instructions_text,
        '#format' => $this->configuration['instructions']['format'],
      ];
    }

    return $instructions;
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $response = $this->_get_peach_response($_POST);

    if ($response->failed) {
      return false;
    }

    $order_checkout_data = $order->getData('peach_checkout');
    $order->setData('peach_checkout', $order_express_checkout_data);
    $order->save();

    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment = $payment_storage->create([
      'state' => 'authorization',
      'amount' => $order->getTotalPrice(),
      'payment_gateway' => $this->entityId,
      'order_id' => $order->id(),
      'remote_id' => $response->checkoutId,
      'remote_state' => $response->result->description,
    ]);

    $payment->state = 'completed';

    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function receivePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorization']);
    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $amount = $this->rounder->round($amount);

    $payment->setState('completed');
    $payment->setAmount($amount);

    // Update the remote id for the captured transaction.
    $payment->setRemoteId('');
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);

    $payment->setState('authorization_voided');
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);

    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);
    $amount = $this->rounder->round($amount);

    $extra['amount'] = $amount->getNumber();
    // Check if the Refund is partial or full.
    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
      $extra['refund_type'] = 'Partial';
    }
    else {
      $payment->setState('refunded');
      if ($amount->lessThan($payment->getAmount())) {
        $extra['refund_type'] = 'Partial';
      }
      else {
        $extra['refund_type'] = 'Full';
      }
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // Delete the remote record here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    // Delete the local entity.
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function updatePaymentMethod(PaymentMethodInterface $payment_method) {
    // Note: Since requires_billing_information is FALSE, the payment method
    // is not guaranteed to have a billing profile. Confirm that
    // $payment_method->getBillingProfile() is not NULL before trying to use it.
    //
    // Perform the update request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    
  }

  public function getRedirectUrl() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function buildAvsResponseCodeLabel($avs_response_code, $card_type) {
    if ($card_type == 'dinersclub' || $card_type == 'jcb') {
      if ($avs_response_code == 'A') {
        return $this->t('Approved.');
      }
      return NULL;
    }
    return parent::buildAvsResponseCodeLabel($avs_response_code, $card_type);
  }

  /**
   * Generate a random string (from DrupalTestCase::randomString).
   */
  public function _commerce_peach_randomstring($length = 8) {
    $str = '';

    for ($i = 0; $i < $length; $i++) {
      $str .= chr(mt_rand(32, 126));
    }

    return $str;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentFlow() {
    return $this->configuration['payment_flow'];
  }
}
