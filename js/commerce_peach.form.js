(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.peachForm = {
    attach: function (context) {
      var peachSettings = drupalSettings.commerce_peach;
      console.log('peachSettings', peachSettings);

      var $paymentForm = $('.payment-redirect-form', context);

      var theform = $('#commerce-checkout-flow-multistep-default');
      theform.attr('data-brands', peachSettings.brands);
      theform.addClass(peachSettings.class);
      theform.attr('action', peachSettings.redirect_url);

      if (peachSettings.complete) {
        theform.trigger('submit');
      }

      console.log('theform', theform);

      var cp = document.createElement('script');
      cp.id = 'PeachCheckoutScript';
      cp.type = 'text/javascript';
      cp.src = peachSettings.peach_url;
      cp.onload = function () {
        // ...
        console.log('peachSettingsOnLoad', 'hello');
      };
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(cp, s);

      $paymentForm.on('submit', function () {
        getpaidSetup(JSON.parse(options));

        return false;
      });

      // Trigger form submission when user visits Payment page.
      $paymentForm.once('getPaid').trigger('submit');
    }
  };

})(jQuery, Drupal, drupalSettings);
